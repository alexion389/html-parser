from html.parser import HTMLParser
content = []
columns = ["name='", "sold_status='", "bedroom_count='", "bathroom_count='"]
headers = ["name", "sold_status", "bedroom_count", "bathroom_count"]

input_file = open('backlog.csv','r')
output_file = open('output.txt', 'w')
output_lines = []
header_line = 'id, project name, timestamp,'
for header in headers:
    header_line = header_line + ' ' + header + ','
output_lines.append(header_line.rstrip(',')+'\n')
print(header_line)

input_lines = input_file.readlines()
input_lines.pop(0)
for line in input_lines:
    output_line = line[:line.find('"')]
    # print(output_line)
    query_string = line[line.find('"')+1:line.rfind('"')]
    for column in columns:
        if query_string.find(column) != -1:
            start = query_string.find(column) + len(column)
            end = query_string.find("'", start)
            value = query_string[start:end]
            # print(value)
            output_line = output_line + ' ' + value + ','
        else:
            output_line = output_line + ' ,'
    output_lines.append(output_line.rstrip(',')+'\n')
    # sold_status = query_string[query_string.find(''):query_string.find(',')-1]
    # query_string = query_string[query_string.find(','):]
    # print()
# print(output_lines)
output_file.writelines(output_lines)
input_file.close()
