import sqlite3

content = []
header_line = 'project_connection_name, bathroom, bedroom, car_space, price'
output_file = open('attribute_names.txt', 'w')
content.append(header_line + '\n')

try:
    src_conn = sqlite3.connect('/Users/junxiongchen/Documents/DisplaySweet/localDB/projects_0208.db')
    cursor = src_conn.cursor()
    # Get the list of target project connections
    result = cursor.execute('select distinct project_connection_name from output')

    connection_list = []

    for row in result:
        row = str(row)
        connection = row[row.find("'") + 1: row.rfind("'")].strip()
        # print(connection)
        connection_list.append(connection)

    for connection in connection_list:
        database_url = '/Users/junxiongchen/Documents/DisplaySweet/localDB/project_folders/%s/database/%s.db' \
                       % (connection, connection)
        # print(database_url)
        print(connection)
        try:
            target_conn = sqlite3.connect(database_url)
            target_cursor = target_conn
            result = target_cursor.execute(
                'select distinct bedroom, bathroom, carpark_space, price from (( select case english when null then '
                '\'empty\' else english end as bedroom from strings where english like \'%bed%\')t1 left outer join( '
                'select english as bathroom from strings where english like \'%bath%\') t2 left outer join(select '
                'english as carpark_space from strings where english like \'%car%\' or english like \'%garage%\') t3 '
                'left outer join(select english as price from strings where english like \'%price%\') t4)')
            has_attribute = False
            for each in result.fetchall():
                has_attribute = True
                result_str = str(each)
                result_str = result_str.replace('(', '').replace(')', '').replace("'", "") + '\n'
                print(connection + ': ' + result_str)
                content.append('%s, %s' % (connection, result_str))
            if not has_attribute:
                content.append('%s, , , \n' % connection)
            has_attribute = False
        except Exception as e:
            print(e)

except Exception as e:
    print(e)

output_file.writelines(content)


