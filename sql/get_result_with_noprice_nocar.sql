SELECT IFNULL(o.project_connection_name, '%s') AS project_connection_name,
       IFNULL(o.project_name, '%s') AS project_name,
       IFNULL(Trim(o.timestamp, ' '), strftime('%%d/%%m/%%Y %%H:%%M', date('now'))) AS timestamp,
       t4.property_id,
       IFNULL(o.sold_status, 'No backlog') AS sold_status,
       t4.property_name,
       t4.bedroom,
       t4.bathroom,
       NULL as car_space,
       NULL as price
FROM
       (SELECT t1.property_id,
               t1.property_name,
               t1.bedroom,
               t2.bathroom
        FROM   (SELECT f.value       AS bedroom,
                       f.property_id AS property_id,
                       n.english     AS property_name
                FROM   properties_string_fields f,
                       strings s,
                       names n,
                       properties p
                WHERE  f.type_string_id = s.id
                       AND s.english = '%s'--bedroom_str
                       AND f.property_id = p.id
                       AND n.id = p.name_id) t1,
               (SELECT f.value       AS bathroom,
                       f.property_id AS property_id,
                       n.english     AS property_name
                FROM   properties_string_fields f,
                       strings s,
                       names n,
                       properties p
                WHERE  f.type_string_id = s.id
                       AND s.english = '%s'--bathroom_str
                       AND f.property_id = p.id
                       AND n.id = p.name_id) t2
        WHERE  t1.property_id = t2.property_id) t4
LEFT OUTER JOIN output o
ON  o.property_id = t4.property_id
      AND o.project_connection_name LIKE '%s'--connection_str
ORDER BY t4.property_id;