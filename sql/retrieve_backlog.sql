(SELECT b.project_id                           AS project_id,
        c.project_connection_name              AS project_connection_name,
        To_char(b.added, 'DD/MM/YYYY HH24:MI') AS timestamp,
        b.log
 FROM   cms_temp_sync.sync_backlog b,
        cms_temp_sync.sync_project_map m,
        PUBLIC.core_project c
 WHERE  ( b.log LIKE '%SET sold_status=%'
          AND b.project_id <> '-1'
          AND b.direction IN ( 1, 4 )
          AND b.project_id = m.new_id
          AND b.project_id = c.id ))
UNION
(SELECT m.old_id                               AS project_id,
        c.project_connection_name              AS project_connection_name,
        To_char(b.added, 'DD/MM/YYYY HH24:MI') AS timestamp,
        b.log                                  AS log
 FROM   cms_temp_sync.sync_backlog b,
        cms_temp_sync.sync_project_map m,
        PUBLIC.core_project c
 WHERE  ( b.log LIKE '%status%'
          AND b.project_id <> '-1'
          AND b.direction NOT IN ( 1, 4 )
          AND b.project_id = m.new_id )
          AND b.project_id = c.id)