from html.parser import HTMLParser
content = []
states = [' Vic ', ' Qld ', ' SA ', ' Tas ', ' NSW ', ' WA ', ' ACT ']


class MyHTMLParser(HTMLParser):

    flag = 0
    projectName = 0
    projectPrice = 0
    projectAddress = 0

    def handle_starttag(self, tag, attrs):
        # print(attrs)
        if tag == 'section':
            for (key, value) in attrs:
                if key == 'class' and 'projectCard' in value:
                    self.flag = 1
            #print(tag)
        if tag == 'h2':
            for (key, value) in attrs:
                if key == 'class' and 'projectName' in value:
                    self.projectName = 1
        if tag == 'p':
            for (key, value) in attrs:
                if key == 'class' and 'projectPrice' in value:
                    self.projectPrice = 1

        if tag == 'p':
            for (key, value) in attrs:
                if key == 'class' and 'projectAddress' in value:
                    self.projectAddress = 1


    def handle_data(self, data):
        if self.projectName == 1:
            content.append('%s\t' % data)
            print('%s,' % data)
            self.projectName = 0
        # if self.projectPrice == 1:
        #     content.append('project price: %s\n' % data)
        #     print('%s\n' % data)
        #     self.projectPrice = 0
        if self.projectAddress == 1:
            content.append('%s\t' % data)
            for state in states:
                if state in data:
                    content.append('%s\n' % state.strip(' '))
            print('%s\n' % data)
            self.projectAddress = 0

    def handle_endtag(self, tag):
        if tag == 'section' and self.flag is 1:
            self.flag = 0
            # print('\n')

parser = MyHTMLParser()
input_file = open('20190301.html','r')
output_file = open('output20190301.txt', 'w')
parser.feed(input_file.read())
output_file.writelines(content)
input_file.close()