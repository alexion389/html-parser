import psycopg2
import sqlite3
from dateutil import parser
from configparser import RawConfigParser


def config(filename='database.ini', section='postgresql'):
    # Create a parser
    parser = RawConfigParser()
    # Read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            # print(param)
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


def connect():
    conn = None
    try:
        # Get connection configuration
        params = config()

        # Attempt connection
        print('Connection to PostgreSql...')
        conn = psycopg2.connect(**params)

        # Create cursor
        cursor = conn.cursor()

        # Execute sql statement
        print('Executing query...')
        # Read sql statement
        fd = open('sql/retrieve_backlog.sql')
        retrieve_back_sql = fd.read()
        cursor.execute(retrieve_back_sql)

        backlog_result = cursor.fetchall()
        # for result in backlog_result:
        #     print(result)

    except (Exception, psycopg2.databaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed')

    if backlog_result is not None:
        try:
            conn = sqlite3.connect('/Users/Patrick/Documents/DisplaySweet/localDB/projects_0208.db')
            # Copy the backlog_table to local database
            print('Start copying result to backlog_result')
            copy_table(None, conn, 'backlog_result', backlog_result)
            # Copy the output table to local project databases
            # Get the list of target project connections
            cursor = conn.cursor()
            result = cursor.execute('select distinct project_connection_name, project_name from output')

            connection_list = []
            # Get all connection_name from output
            print('start copying data in output to project dbs')
            for row in result:
                row = str(row)
                connection_details = row.split(',')
                connection = connection_details[0].replace('\'', '')[1:]
                project_name = connection_details[1].replace('\'', '')[:connection_details[1].rfind(')') - 1].strip()
                # print(connection)
                connection_list.append(connection)

            # Link the databases based on the connection name
                database_url = '/Users/Patrick/Documents/DisplaySweet/localDB/project_folders/%s/database/%s.db' \
                               % (connection, connection)
                target_conn = sqlite3.connect(database_url)
                copy_table(conn, target_conn, 'output', None)
                # Get result
                try:
                    '''
                        Start to query the result according to the attribute names
                    '''
                    cursor = target_conn.cursor()
                    start = 0
                    for line in attribute_names:
                        # skip the header
                        if start > 0:
                            # print(line)
                            line_connection = line[:line.find(',')]
                            if line_connection == connection:
                                # print(line_connection)
                                bedroom_index = line.find(',') + 2
                                bedroom_str = line[
                                              bedroom_index: line[bedroom_index:].find(',') + bedroom_index].rstrip()
                                # print(bedroom_str)
                                bathroom_index = bedroom_index + len(bedroom_str) + 2
                                bathroom_str = line[bathroom_index: line[bathroom_index:].find(
                                    ',') + bathroom_index].rstrip()
                                # print(bathroom_str)
                                car_space_index = bathroom_index + len(bathroom_str) + 2
                                car_space_str = line[
                                                car_space_index: line[car_space_index:].find(',') + car_space_index] \
                                    .rstrip()
                                # print(car_space_str)
                                price_index = car_space_index + len(car_space_str) + 2
                                price_str = line[price_index:].rstrip()
                                # print(price_str)
                                connection_str = '%' + connection + '%'
                                if bedroom_str == '' or bathroom_str == '':
                                    break

                                # Generate query based on car_space_str and price_str
                                if price_str != 'None' and len(price_str) > 1:
                                    if car_space_str != 'None' and car_space_str != '':
                                        # Car_space_str && no price
                                        sql_file = open('sql/get_result_with_price_car.sql')
                                        query_str = sql_file.read() % (connection,
                                                                       project_name, bedroom_str,
                                                                       bathroom_str, car_space_str,
                                                                       price_str, connection_str)
                                        print(query_str)
                                    else:
                                        # Car_space_str && price
                                        sql_file = open('sql/get_result_with_price_nocar.sql')
                                        query_str = sql_file.read() % (connection,
                                                                       project_name,
                                                                       bedroom_str, bathroom_str, price_str,
                                                                       connection_str)
                                        print(query_str)
                                else:
                                    if car_space_str != 'None' and car_space_str != '':
                                        # Car_space_str && no price
                                        sql_file = open('sql/get_result_with_car_noprice.sql')
                                        query_str = sql_file.read() % (connection,
                                                                       project_name,
                                                                       bedroom_str, bathroom_str, car_space_str,
                                                                       connection_str)
                                        print(query_str)
                                    else:
                                        # no Car_space_str && no price
                                        sql_file = open('sql/get_result_with_noprice_nocar.sql')
                                        query_str = sql_file.read() % (connection,
                                                                       project_name,
                                                                       bedroom_str, bathroom_str, connection_str)
                                        print(query_str)
                                query_result = cursor.execute(query_str)
                                query_result = remove_duplicated_results(query_result)
                                # Remove duplicated records
                                for each_line in query_result:
                                    each_line = str(each_line)
                                    each_line = each_line.replace('(', '').replace(')', '').replace("'", "") + '\n'
                                    # Remove comma from price
                                    price_start = each_line.find('$')
                                    price = each_line[price_start:].replace(',', '')
                                    # print(price)
                                    each_line = each_line[:price_start] + price
                                    content.append(each_line)
                                    print(each_line)
                                break
                        start += 1
                except Exception as e:
                    print(e)
                    # print(query_str)
            # Write file
            print('Writing output file...')
            output_file.writelines(content)
        except Exception as e:
            print(e)
        finally:
            if conn is not None:
                conn.close()


def remove_duplicated_results(query_result):
    last_project = ''
    last_timestamp = ''
    last_property_id = ''
    last_line = ''
    not_duplicated_result = []

    for result in query_result:
        print('===========================')
        print(str(result))
        words = str(result).split(',')
        project = words[0]
        timestamp = parser.parse(words[2])
        property_id = words[3]
        if project != last_project:
            last_project = project
            last_timestamp = timestamp
            last_property_id = property_id
            not_duplicated_result.append(result)
            last_line = result
            continue
        if property_id != last_property_id:
            last_property_id = property_id
            last_timestamp = timestamp
            not_duplicated_result.append(result)
            last_line = result
        else:
            if timestamp > last_timestamp:
                last_line = result

        # print(project)
        # print(timestamp)
        # print(property_id)
    return not_duplicated_result


def copy_table(src, target, table_name, data):
    # If src is None, then copy the result from backlog to projects database
    if src is not None:
        src_cursor = src.cursor()
        table_to_copy = src_cursor.execute('select * from %s' % table_name)
        target_cursor = target.cursor()
        drop_table = 'drop table if exists output'
        target_cursor.execute(drop_table)
        create_table = 'create table output (project_connection_name varchar, project_name varchar, \
                        timestamp varchar, property_id varchar, sold_status varchar)'
        target_cursor.execute(create_table)

        for item in table_to_copy.fetchall():
            cols = 'project_connection_name, project_name, timestamp, property_id, sold_status'
            ins_sql = 'insert or replace into %s (%s) values%s' % (table_name, cols, item)
            #print('Insert clause: ' + ins_sql)
            try:
                target_cursor.execute(ins_sql)
            except Exception as e:
                print('Error when inserting into backlog_result, sql=' + ins_sql)
                print(e)
        target.commit()
    else:
        drop_table = 'drop table if exists ' + table_name
        cursor = target.cursor()
        cursor.execute(drop_table)
        create_table = 'create table backlog_result (timestamp varchar, project_id varchar, log varchar, \
                        project_connection_name varchar)'
        cursor.execute(create_table)

        for item in data:
            cols = 'project_id, project_connection_name, timestamp, log'
            ins_sql = 'insert or replace into {0} ({1}) values{2}'.format(table_name, cols, item)
            # print(ins_sql)

            try:
                cursor.execute(ins_sql)
            except Exception as e:
                print('Error when inserting into backlog_result, sql=' + ins_sql)
                print(e)
        target.commit()
        get_output(target)


def get_output(conn):
    print('start getting output')
    # Match the string in log field
    columns = ["property_id=", "SET sold_status=",
               "WHERE type_string_id IN (SELECT id FROM strings WHERE english='status')"]

    # Remove data from table output
    drop_table = 'drop table if exists output'
    cursor = conn.cursor()
    cursor.execute(drop_table)
    create_table = 'create table output (project_connection_name varchar, project_name varchar, timestamp varchar,' \
                   'property_id varchar, sold_status varchar)'
    cursor.execute(create_table)

    # Join table projects with backlog_result to get project names
    join_sql = 'SELECT b.project_connection_name, p.name as project_name, b.timestamp, b.log ' \
               'from backlog_result b, projects p where b.project_id = p.id;'

    cursor = conn.cursor()
    try:
        cursor.execute(join_sql)
        result = cursor.fetchall()
    except Exception as e:
        print('Query project name error')
        print(e)

    for item in result:
        # print(item)
        '''
            Parse the result and get property id, sold_status, price then insert the result into table output
            --- Start
        '''
        project_connection = '\'' + item[0] + '\''
        project = '\'' + item[1] + '\''
        # print(project_connection)
        timestamp = '\'' + item[2] + '\''
        # print(timestamp)
        update_log = '\'' + item[3] + '\''
        if update_log.find(columns[0]) != -1:
            property_id = update_log[update_log.find(columns[0]) + len(columns[0]):]
            # set sold_status
            if update_log.find(columns[1]) > -1:
                sold_status_index = update_log.find(columns[1]) + len(columns[1]) + 1
                end_position = update_log[sold_status_index:].find('\'') + sold_status_index
                sold_status = update_log[sold_status_index: end_position]
            else:
                sold_status_index = update_log[:update_log.find(columns[2])].find('SET value=') + len('SET value=') + 1
                end_position = update_log[sold_status_index:].find('\'') + sold_status_index
                sold_status = update_log[sold_status_index: end_position]
            # Ftab(sold_status)
            if len(property_id) < 5 and len(sold_status) < 12:
                # Line the attribute values
                output_line = project_connection.strip() + ', '
                output_line = output_line + project.strip() + ', '
                output_line = output_line + timestamp.strip() + ', '
                output_line = output_line + '\'' + property_id.strip('\n') + ', '
                output_line = output_line + '\'' + sold_status.strip('\n').strip('\r') + '\''
                if project != '':
                    # print(output_line)
                    # Insert into output
                    ins_sql = 'insert or replace into output (project_connection_name, project_name, timestamp, ' \
                              'property_id, sold_status) values({0})'.format(output_line)
                    #print(ins_sql)
                    try:
                        cursor.execute(ins_sql)
                    except Exception as e:
                        print(e)
                        print('Error when inserting into output')
            else:
                print('property id: ' + property_id)
                print('sold_status: ' + sold_status)
        '''
            --- End
        '''
    conn.commit()


if __name__ == '__main__':

    filtered_attribute_names = open('new_filtered_attribute_names.csv', 'r')
    content = []
    output_file = open('property_result.txt', 'w')

    attribute_names = filtered_attribute_names.readlines()
    # print(attribute_names)
    header_line = 'project_connection_name, project_name, timestamp, property_id, sold_status, property_name, ' \
                  'bedroom, bathroom, car_space, price'
    content.append(header_line + '\n')
    connect()

