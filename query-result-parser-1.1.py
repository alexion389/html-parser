content = []
columns = ["property_id=", "SET sold_status=",
           "WHERE type_string_id IN (SELECT id FROM strings WHERE english='status')"]
append_headers = ["property_id", "sold_status"]

input_file = open('input.csv', 'r')
output_file = open('output.txt', 'w')
header_line = 'project_connection_name, project_name, timestamp'
for header in append_headers:
    header_line = header_line + ', ' + header
content.append(header_line + '\n')
# print(header_line)

input_lines = input_file.readlines()
input_lines.pop(0)
for line in input_lines:
    # print(output_line)
    project_connection = line[:line.find(',')]
    project_start = line.find(',') + 1
    project = line[project_start:line[project_start:].find(',') + project_start]
    # print(project)
    timestamp_start = line[project_start:].find(project) + project_start + len(project) + 1
    timestamp_end = line[timestamp_start:].find(',') + timestamp_start
    # print(timestamp_start)
    timestamp = line[timestamp_start: timestamp_end]
    print(timestamp)
    if line.find(columns[0]) != -1:
        property_id = line[line.find(columns[0]) + len(columns[0]):]
        # set sold_status
        if line.find(columns[1]) > -1:
            sold_status_index = line.find(columns[1]) + len(columns[1]) + 1
            end_position = line[sold_status_index:].find('\'') + sold_status_index
            sold_status = line[sold_status_index: end_position]
        else:
            sold_status_index = line[:line.find(columns[2])].find('SET value=') + len('SET value=') + 1
            end_position = line[sold_status_index:].find('\'') + sold_status_index
            sold_status = line[sold_status_index: end_position]
        # Ftab(sold_status)
        if len(property_id) < 5 and len(sold_status) < 10:
            # if property_id is not None and timestamp is not None and project is not None:
            #     content.append('%s, %s, %s\n' % timestamp, project, property_id, sold_status)
            #     print('%s, %s, %s\n' % timestamp, project, property_id, sold_status)
            # print(timestamp)
            # print(project)
            # try:
            #
            # except Exception as e:
            #     print(e)
            output_line = project_connection.strip() + ', '
            output_line = output_line + project.strip() + ', '
            output_line = output_line + timestamp.strip() + ', '
            output_line = output_line + property_id.strip('\n') + ', '
            output_line = output_line + sold_status.strip('\n').strip('\r')
            if project != '':
                content.append(output_line + '\n')
            # print(output_line)

        else:
            print(property_id)
    # if project != '':
    #     content.append(output_line + '\n')
    # if output_line.count(',') > 1:
    #     print(output_line.count(','))
    #     print('*******')
    #     print(output_line)
# print(output_lines)
output_file.writelines(content)
input_file.close()
