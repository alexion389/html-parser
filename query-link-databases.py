import sqlite3
from datetime import datetime

filtered_attribute_names = open('new_filtered_attribute_names.csv', 'r')
content = []
output_file = open('property_result.txt', 'w')

attribute_names = filtered_attribute_names.readlines()
# print(attribute_names)
header_line = 'project_connection_name, project_name, timestamp, property_id, sold_status, property_name, bedroom, ' + \
              'bathroom, car_space, price'


def copy_table(src, target):
    table_name = 'output'
    src_cursor = src.cursor()
    table_to_copy = src_cursor.execute('select * from %s' % table_name)
    target_cursor = target.cursor()
    drop_table = 'drop table if exists output'
    target_cursor.execute(drop_table)
    create_table = 'create table output (project_connection_name varchar, project_name varchar, \
                    timestamp varchar, property_id varchar, sold_status varchar)'
    target_cursor.execute(create_table)

    for item in table_to_copy.fetchall():
        cols = 'project_connection_name, project_name, timestamp, property_id, sold_status'
        ins_sql = 'insert or replace into %s (%s) values%s' % (table_name, cols, item)
        # print('Insert clause: ' + ins_sql)
        target_cursor.execute(ins_sql)

    target.commit()


try:
    src_conn = sqlite3.connect('/Users/junxiongchen/Documents/DisplaySweet/localDB/projects_0208.db')
    cursor = src_conn.cursor()
    # Get the list of target project connections
    result = cursor.execute('select distinct project_connection_name from output')

    connection_list = []

    # Get all connection_name from output
    for row in result:
        row = str(row)
        connection = row[row.find("'") + 1: row.rfind("'")].strip()
        # print(connection)
        connection_list.append(connection)

    # Link the databases based on the connection name
    for connection in connection_list:
        database_url = '/Users/junxiongchen/Documents/DisplaySweet/localDB/project_folders/%s/database/%s.db' \
                       % (connection, connection)
        # print(database_url)

        try:
            target_conn = sqlite3.connect(database_url)
            query_cursor = target_conn.cursor()
            # result = query_cursor.execute('select * from names')
            # print(result.fetchall())
            copy_table(src_conn, target_conn)

            '''
                Start to query the result according to the attribute names
            '''
            start = 0
            for line in attribute_names:
                # skip the header
                if start > 0:
                    # print(line)
                    line_connection = line[:line.find(',')]
                    if line_connection == connection:
                        # print(line_connection)
                        bedroom_index = line.find(',') + 2
                        bedroom_str = line[bedroom_index: line[bedroom_index:].find(',') + bedroom_index].rstrip()
                        # print(bedroom_str)
                        bathroom_index = bedroom_index + len(bedroom_str) + 2
                        bathroom_str = line[bathroom_index: line[bathroom_index:].find(',') + bathroom_index].rstrip()
                        # print(bathroom_str)
                        car_space_index = bathroom_index + len(bathroom_str) + 2
                        car_space_str = line[car_space_index: line[car_space_index:].find(',') + car_space_index]\
                            .rstrip()
                        # print(car_space_str)
                        price_index = car_space_index + len(car_space_str) + 2
                        price_str = line[price_index:].rstrip()
                        # print(price_str)
                        connection_str = '%' + connection + '%'
                        if bedroom_str == '' or bathroom_str == '':
                            break
                        format_string = r'%d/%m/%Y %H:%M'
                        # Check null for car_space_str
                        if price_str != 'None' and len(price_str) > 1:
                            select_price_str = ', (SELECT f.value as price, f.property_id as property_id, n.english \
                                                as property_name from properties_string_fields f, strings s, names n, \
                                                properties p where f.type_string_id = s.id and s.english = \'%s\' and \
                                                f.property_id = p.id and n.id = p.name_id) t5' % price_str
                        else:
                            select_price_str = ', (SELECT \'None\' as price, f.property_id as property_id, n.english \
                                                as property_name from properties_string_fields f, strings s, names n, \
                                                properties p where f.type_string_id = s.id and\
                                                f.property_id = p.id and n.id = p.name_id) t5'

                        # print(select_price_str)
                        if car_space_str != 'None' and car_space_str != '':
                            query_str = 'select o.project_connection_name, o.project_name, \
                                        strftime(\'' + format_string + '\', datetime(trim(o.timestamp, \' \'))) as ' \
                                                                       'timestamp,\
                                        o.property_id, o.sold_status, t4.property_name, t4.bedroom, t4.bathroom, ' \
                                                                       't4.car_space, t4.price from\
                                        output o, ( select t1.property_id, t1.property_name, t1.bedroom, t2.bathroom,\
                                        t3.car_space, t5.price from (SELECT f.value as bedroom, f.property_id as\
                                        property_id,\
                                        n.english as property_name from properties_string_fields f, strings s,\
                                        names n, properties p where f.type_string_id = s.id and s.english = \'' \
                                        + bedroom_str + '\' and\
                                        f.property_id = p.id and n.id = p.name_id) t1, (SELECT f.value as bathroom,\
                                        f.property_id as property_id, n.english as property_name\
                                        from properties_string_fields f, strings s, names n, properties p\
                                        where f.type_string_id = s.id and s.english = \'' + bathroom_str + '\' and\
                                        f.property_id = p.id and n.id = p.name_id) t2,\
                                        (SELECT f.value as car_space, f.property_id as property_id, \
                                        n.english as property_name from properties_string_fields f,\
                                        strings s, names n, properties p where f.type_string_id = s.id\
                                        and s.english = \'' + car_space_str + '\'\
                                        and f.property_id = p.id and n.id = p.name_id) t3'\
                                        + select_price_str + ' where t1.property_id = t2.property_id and t1.property_id\
                                        = t3.property_id and \
                                        t1.property_id = t5.property_id) t4\
                                        where o.property_id = t4.property_id and o.project_connection_name\
                                        like \'' + connection_str + '\''
                            # print(query_str)
                        else:
                            query_str = 'select o.project_connection_name, o.project_name, \
                                        strftime(\'' + format_string + '\', datetime(trim(o.timestamp, \' \'))) as ' \
                                                                       'timestamp,\
                                        o.property_id, o.sold_status, t4.property_name, t4.bedroom, t4.bathroom from\
                                        output o, ( select t1.property_id, t1.property_name, t1.bedroom, t2.bathroom\
                                        from (SELECT f.value as bedroom, f.property_id as property_id,\
                                        n.english as property_name from properties_string_fields f, strings s,\
                                        names n, properties p where f.type_string_id = s.id and s.english = \'' \
                                        + bedroom_str + '\' and\
                                        f.property_id = p.id and n.id = p.name_id) t1, (SELECT f.value as bathroom,\
                                        f.property_id as property_id, n.english as property_name\
                                        from properties_string_fields f, strings s, names n, properties p\
                                        where f.type_string_id = s.id and s.english = \'' + bathroom_str + '\' and\
                                        f.property_id = p.id and n.id = p.name_id) t2'\
                                        + select_price_str + \
                                        ' where t1.property_id = t2.property_id and t1.property_id = t5.property_id) t4\
                                        where o.property_id = t4.property_id and o.project_connection_name\
                                        like \'' + connection_str + ''
                            # print(query_str)
                        query_result = query_cursor.execute(query_str)
                        for each_line in query_result:
                            each_line = str(each_line)
                            each_line = each_line.replace('(', '').replace(')', '').replace("'", "") + '\n'
                            # Remove comma from price
                            price_start = each_line.find('$')
                            price = each_line[price_start:].replace(',', '')
                            # print(price)
                            each_line = each_line[:price_start] + price
                            content.append(each_line)
                            print(each_line)
                        break
                start += 1
        except Exception as e:
            print(e)
            # print(query_str)
except Exception as e:
    print(e)

output_lines = []
last_project = ''
last_timestamp = ''
last_property_id = ''
last_line = ''

output_lines.append(header_line.rstrip(',')+'\n')

for line in content:

    # print(line)
    words = line.split(', ')
    project = words[0]
    timestamp = datetime.strptime(words[2].strip(), '%d/%m/%Y %H:%M')
    property_id = words[3]
    if project != last_project:
        last_project = project
        last_timestamp = timestamp
        last_property_id = property_id
        output_lines.append(last_line)
        last_line = line
        continue
    if property_id != last_property_id:
        last_property_id = property_id
        last_timestamp = timestamp
        output_lines.append(last_line)
        last_line = line

    else:
        if timestamp > last_timestamp:
            last_line = line

    # print(project)
    # print(timestamp)
    # print(property_id)

output_file.writelines(output_lines)
filtered_attribute_names.close()
