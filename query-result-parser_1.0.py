from html.parser import HTMLParser
content = []
columns = ["property_id="]
append_headers = ["property_id"]

input_file = open('new_result.csv','r')
output_file = open('output.txt', 'w')
output_lines = []
header_line = ' timestamp, project name,'
for header in append_headers:
    header_line = header_line + ' ' + header + ','
output_lines.append(header_line.rstrip(',')+'\n')
# print(header_line)

input_lines = input_file.readlines()
input_lines.pop(0)
i = 1
for line in input_lines:
    # print(line)
    start = line.find(',')
    indexOfSecComa = line.find(',', start + 1)
    # print(indexOfSecComa)
    output_line = line[:indexOfSecComa]
    # print(output_line)
    query_string = line[line.find('"')+1:line.rfind('"')]
    # query_string = query_string.replace(' ', '')
    # query_string = query_string.replace('\n', ' ')
    # query_string = query_string.replace('\r', ' ')
    # test_query = query_string.replace('\r', ' ')
    if i < 1000:
        print(query_string)
    i += 1
    for column in columns:
        # print(query_string)
        # print(column)
        if query_string.find(column) != -1 and len(query_string) > 150:
            start = query_string.find(column) + len(column)
            end = query_string.find("'", start)
            value = query_string[start:]
            # print(value)
            output_line = output_line + ', ' + value + ','
        else:
            # print(output_line)
            output_line = output_line + ' ,'
    output_lines.append(output_line.rstrip(',')+'\n')
    # sold_status = query_string[query_string.find(''):query_string.find(',')-1]
    # query_string = query_string[query_string.find(','):]
    # print()
# print(output_lines)
output_file.writelines(output_lines)
input_file.close()
